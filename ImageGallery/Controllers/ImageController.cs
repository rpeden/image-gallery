﻿using ImageGallery.Data;
using ImageGallery.Models;
using ImageGallery.Models.ViewModels;
using ImageMagick;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ImageGallery.Controllers
{
    public class ImageController : Controller
    {
        private ImageGalleryDbContext db = new ImageGalleryDbContext();
        private Random random = new Random();

        // GET: Image
        public ActionResult Index()
        {
            var images = db.Images.ToList();
            ViewBag.Images = images;
            return View();
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Add(ImageUploadViewModel model)
        {
            var fileNames = SaveImage(model.ImageFile);

            var image = new GalleryImage
            {
                Title = model.Title,
                Description = model.Description,
                ThumbnailFileName = fileNames.thumbFileName,
                FileName = fileNames.fileName
            };

            db.Images.Add(image);
            await db.SaveChangesAsync();

            return Redirect("/Image");
        }

        // GET: Image/Details/5
        public ActionResult Details(int id)
        {
            var imageDetails = db.Images.Find(id);
            ViewBag.Image = imageDetails;
            return View();
        }

        // GET: Image/Delete/5
        public ActionResult Delete(int id)
        {
            var image = db.Images.Find(id);
            if (image != null)
            {
                var path = Server.MapPath("/Content/Images");
                var thumbPath = Path.Combine(path, image.ThumbnailFileName);
                var imagePath = Path.Combine(path, image.FileName);

                if (System.IO.File.Exists(thumbPath))
                {
                    System.IO.File.Delete(thumbPath);
                }

                if (System.IO.File.Exists(imagePath))
                {
                    System.IO.File.Delete(imagePath);
                }

                db.Images.Remove(image);
                db.SaveChanges();   
            }

            return RedirectToAction("Index");
        }

        private (string thumbFileName, string fileName) SaveImage(HttpPostedFileBase imageData)
        {
            var path = Server.MapPath("/Content/Images");
            Directory.CreateDirectory(path);
            var fileName = RandomString(10);

            using (MagickImage image = new MagickImage(imageData.InputStream))
            {
                var thumbnail = image.Clone();
                var desiredWidth = 320;
                var thumbnailRatio = thumbnail.Width / desiredWidth;
                var thumbnailWidth = thumbnail.Width / thumbnailRatio;
                var thumbnailHeight = thumbnail.Height / thumbnailRatio;
                thumbnail.Resize(thumbnailWidth, thumbnailHeight);
                var thumbnailFileName = $"{fileName}_thumb.jpg";
                var thumbnailFilePath = Path.Combine(path, thumbnailFileName);
                //thumbnail.Orientation = OrientationType.Undefined;
                thumbnail.Write(thumbnailFilePath);

                var fullFileName = $"{fileName}.jpg";
                var fullFilePath = Path.Combine(path, fullFileName);
                //image.Orientation = OrientationType.Undefined;
                image.Write(fullFilePath);

                return (thumbnailFileName, fullFileName);
            }
        }

        // from https://stackoverflow.com/questions/1344221
        public string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
