namespace ImageGallery.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddThumbnail : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.GalleryImages", "ThumbnailFileName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.GalleryImages", "ThumbnailFileName");
        }
    }
}
