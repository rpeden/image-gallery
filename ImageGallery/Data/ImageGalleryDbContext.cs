﻿using ImageGallery.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ImageGallery.Data
{
    public class ImageGalleryDbContext : DbContext
    {
        public ImageGalleryDbContext() : base("ImageGallery") { }

        public DbSet<GalleryImage> Images { get; set; }
    }
}